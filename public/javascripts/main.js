$(document).ready(function() {
  var cartCount;
  var $cart = $('#cart');
  var $cartCount = $('.cart-count');

  $('.add-btn').click(function(e) {
    e.preventDefault;
    if ($(this).hasClass('added')) return false;
    $(this).addClass('added');
    if ($cart.hasClass('active')) {
      $cartCount.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
        function(e) {
          if ($cartCount.hasClass('cart-pop')) {
            $(this).html((parseInt($cartCount.html(), 10) || 0) + 1);
            $(this).removeClass('cart-pop');
          }
        });
      $cartCount.addClass('cart-pop');
    } else {
      $cart.addClass('active');
    }
  });
});